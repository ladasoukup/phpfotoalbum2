<?php
//////////////////////////////////
// phpFotoAlbum2 language file  //
//         ---> ES <---         //
// by Imasce  [jorge@imasce.es] //
//////////////////////////////////

$content_type="<meta http-equiv=\"content-type\" content=\"text/xhtml; charset=windows-1251\" />\n";
$content_language="<meta http-equiv=\"content-language\" content=\"es\" />\n";
$str["date_format"]="d.m.Y H:i:s";

$str["menu_show"]="Ver como";
$str["menu_show_list"]="Lista";
$str["menu_show_thumb"]="Miniaturas";

$str["menu_sort"]="Ordenar por";
$str["menu_sort_name"]="Nombre";
$str["menu_sort_type"]="Tipo";
$str["menu_sort_time"]="Fecha";
$str["menu_slideshow"]="Presentación";
$str["menu_setup"]="Configuración";

$str["menu_asc"]="Ascendiente";
$str["menu_desc"]="Descendiente";

$str["menu2_prev"]="Anterior";
$str["menu2_next"]="Siguiente";
$str["menu2_up"]="[ ATRAS ]";
$str["menu2_exit"]="Salir";

$str["dir"]="Carpeta";
$str["list_root"]="[ . ]";
$str["list_up"]="[ .. ]";
$str["list_unknown"]="-???-";
$str["count"]="Total carpetas: %1, total archivos: %2 (%3)";

$str["setup"]="Configuración";
$str["setup_skin"]="Aspecto:";
$str["setup_lang"]="Idioma:";
$str["setup_res"]="Resolución:";
$str["setup_quality"]="Calidad:";
$res["orig"]="original";
$res["640x480"]="pequeña (640x480)";
$res["800x600"]="media (800x600)";
$res["1024x768"]="grande (1024x768)";
$res["1280x1024"]="extra (1280x1024)";
$str["setup_submit"]="Aceptar";

$str["download_full_res"]="Descargar en alta resolución.";

$str["error_working"]="Error de trabajo ...";
$str["error_dir"]="Directorio erróneo !!!";
$str["error_listing"]="Error de listado ...";
?>