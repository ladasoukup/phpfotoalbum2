<?php
//////////////////////////////////
// phpFotoAlbum2 language file  //
//         ---> NL <---         //
//      made by kierownik       //
//////////////////////////////////

$content_type="<meta http-equiv=\"content-type\" content=\"text/xhtml; charset=windows-1251\" />\n";
$content_language="<meta http-equiv=\"content-language\" content=\"nl\" />\n";
$str["date_format"]="d.m.Y H:i:s";

$str["menu_show"]="Bekijk als";
$str["menu_show_list"]="Lijst";
$str["menu_show_thumb"]="voorbeeld";

$str["menu_sort"]="Sorteer op";
$str["menu_sort_name"]="Naam";
$str["menu_sort_type"]="Type";
$str["menu_sort_time"]="Tijd";
$str["menu_slideshow"]="slideshow";
$str["menu_setup"]="Opzetten";

$str["menu_asc"]="naar beneden";
$str["menu_desc"]="naar boven";

$str["menu2_prev"]="vorige";
$str["menu2_next"]="volgende";
$str["menu2_up"]="[ terug ]";
$str["menu2_exit"]="eruit";

$str["dir"]="Map";
$str["list_root"]="[ . ]";
$str["list_up"]="[ .. ]";
$str["list_unknown"]="-???-";
$str["count"]="Totaal aantal mappen: %1, totaal bestanden: %2 (%3)";

$str["setup"]="Opstelling";
$str["setup_skin"]="Skin:";
$str["setup_lang"]="Taal:";
$str["setup_res"]="Resolutie:";
$str["setup_quality"]="Kwaliteit:";
$res["orig"]="Origineel";
$res["640x480"]="Klein (640x480)";
$res["800x600"]="Gemiddeld (800x600)";
$res["1024x768"]="Groot (1024x768)";
$res["1280x1024"]="Extra (1280x1024)";
$str["setup_submit"]="Submit";

$str["download_full_res"]="Download in volledige resolutie.";

$str["error_working"]="Deze functie kan niet gedaan worden ...";
$str["error_dir"]="Verkeerd adres !!!";
$str["error_listing"]="Verkeerde lijst methode ...";

$str["adm_user_login"]="Administrator login"; //NEED TO BE TRANSLATED
?>