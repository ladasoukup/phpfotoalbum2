<?php
//////////////////////////////////
// phpFotoAlbum2 language file  //
//         ---> CZ <---         //
//////////////////////////////////
$content_type="<meta http-equiv=\"content-type\" content=\"text/xhtml; charset=windows-1250\" />\n";
$content_language="<meta http-equiv=\"content-language\" content=\"cs\" />\n";
$str["date_format"]="d.m.Y H:i:s";

$str["menu_show"]="Zobrazen�";
$str["menu_show_list"]="seznam";
$str["menu_show_thumb"]="n�hledy";
$str["menu_sort"]="�adit podle";
$str["menu_sort_name"]="jm�na";
$str["menu_sort_type"]="p��pony";
$str["menu_sort_time"]="�asu";
$str["menu_slideshow"]="slideshow";
$str["menu_setup"]="-nastaven�-";

$str["menu2_prev"]="p�ede�l�";
$str["menu2_next"]="n�sleduj�c�";
$str["menu2_up"]="[ ZP�T ]";
$str["menu2_exit"]="zav��t";

$str["dir"]="adres��";
$str["list_root"]="[ . ]";
$str["list_up"]="[ .. ]";
$str["list_unknown"]="-???-";
$str["count"]="Po�et adres���: %1, po�et soubor�: %2 (%3)";

$str["setup"]="Nastaven�";
$str["setup_skin"]="Zvolte si skin phpFotoAlba:";
$str["setup_lang"]="Zvolte jazyk phpFotoAlba:";
$str["setup_res"]="Zvolte rozli�en� pro prohl�en� obr�zk�: ";
$str["setup_quality"]="Zvolte kvalitu pro prohl�en� obr�zk�: ";
$res["orig"]="p�vodn� rozli�en�";
$res["640x480"]="mal� (640x480)";
$res["800x600"]="st�edn� (800x600)";
$res["1024x768"]="velk� (1024x768)";
$res["1280x1024"]="extra (1280x1024)";
$str["setup_submit"]="ulo�it";

$str["download_full_res"]="St�hnout v p�vodn�m rozli�en�.";

$str["error_working"]="Tato funkce zat�m nen� implementov�na....";
$str["error_dir"]="NEPLATN� ADRES��!!!";
$str["error_listing"]="NEPODPOROVAN� ZP�SOB ZOBRAZEN�...";

$str["access_denied"]="Nem�te povolen p��stup k t�to funkci.";

$str["adm_continue"]="Pokra�ovat...";
$str["adm_storno"]="Zru�it...";
$str["adm_yes"]="Ano";
$str["adm_no"]="Ne";
$str["adm_user_login"]="P�ihl�en� do administrace";
$str["adm_user_logout"]="Odhl�en� z administrace";
$str["adm_userid"]="U�ivatel";
$str["adm_userpass"]="Heslo";
$str["adm_userlogin_btn"]=" P�ihl�sit ";
$str["adm_login_failed"]="Chybn� jm�no nebo heslo!";
$str["adm_login_ok"]="P�ihl�en� bylo �sp�n�.";
$str["adm_logout_ok"]="Odhl�en� bylo �sp�n�.";
$str["adm_new_folder"]="Nov� slo�ka";
$str["adm_new_folder_name"]="N�zev slo�ky";
$str["adm_new_folder_submit"]="Vytvo�";
$str["adm_new_folder_ok"]="Adres�� \"%1\" byl �sp�n� vytvo�en.";
$str["adm_new_folder_error"]="Adres�� \"%1\" se NEPODA�ILO vytvo�it.";
$str["adm_new_files"]="Nov� fotky";
$str["adm_new_file_name"]="Cesta k obr�zku";
$str["adm_new_file_res"]="Zm�nit rozli�en� na: ";
$str["adm_new_file_submit"]="Nahraj";
$str["adm_new_file_ok"]="Soubor \"%1\" byl �sp�n� nahr�n.";
$str["adm_new_file_error"]="Soubor \"%1\" se NEPODA�ILO nahr�t.";
$str["adm_del_thumbs"]="Smazat n�hledy";
$str["adm_del_thumbs_subdirs"]="Smazat i v podslo�k�ch?";
$str["adm_del_thumbs_ok"]="Soubor \"%1\" byl �sp�n� smaz�n.";
$str["adm_del_thumbs_error"]="Soubor \"%1\" NEBYL smaz�n.";
?>