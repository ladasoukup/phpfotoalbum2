<?php
include "./adm_check_login.php";
if ($_SESSION["user"]["create_dir"]){
	if (empty($_POST)){
		// form
		echo "<h1>".$str["adm_new_folder"]."</h1>\n";
		echo "<form action=\"index.php?page=adm_new_folder\" method=\"post\" name=\"new_folder\" class=\"form_out\">\n";
		echo $str["adm_new_folder_name"].": \n";
		echo "<input type=\"text\" name=\"folder_name\" class=\"form\" /><br />\n";
		echo "<br /><input type=\"submit\" name=\"submit\" value=\"".$str["adm_new_folder_submit"]."\" class=\"form\" /><br />\n";
		echo "</form><br />\n";
		echo "<a href=\"index.php?page=\">".$str["adm_storno"]."</a><br /><br />\n";
	} else {
		// create...
		$new_dir="./_images".$_SESSION["s_data"]["dir"].$_POST["folder_name"];
		if (@mkdir($new_dir, 0777)){
			$_SESSION["s_data"]["dir"]=$_SESSION["s_data"]["dir"].$_POST["folder_name"]."/";
			$str["adm_new_folder_ok"]=str_replace("%1",$new_dir,$str["adm_new_folder_ok"]);
			echo $str["adm_new_folder_ok"];
		} else {
			$str["adm_new_folder_error"]=str_replace("%1",$new_dir,$str["adm_new_folder_error"]);
			echo $str["adm_new_folder_error"];
		}
		echo "<h1><a href=\"index.php?page=\">".$str["adm_continue"]."</a></h1>\n";
		
	}
} else {
	echo "<div class=\"error\"> ".$str["access_denied"]." </div>";
	echo "<a href=\"index.php?page=\">".$str["adm_continue"]."</a>\n";
}
?>