<?php
include "./adm_check_login.php";
set_time_limit(0);
$max=10; //max files
if (empty($_POST)){
	// form
	echo "<h1>".$str["adm_new_files"]."</h1>\n";
	echo "<form action=\"index.php?page=adm_new_file\" method=\"post\" enctype=\"multipart/form-data\" name=\"new_folder\" class=\"form_out\">\n";
	for($i=0;$i<$max;$i++){
		echo ($i+1).". ".$str["adm_new_file_name"].": \n";
		echo "<input type=\"file\" name=\"file_name_".$i."\" /><br />\n";
	}
	echo $str["adm_new_file_res"];
	echo "<select name=\"new_res\" class=\"form\">\n";
	foreach($res as $var=>$val){
		if (Trim($_SESSION["s_data"]["upload_res"])==$var){
			echo "<option value=\"".$var."\" SELECTED>".$val."</option>\n";
		} else {
			echo "<option value=\"".$var."\">".$val."</option>\n";
		}
	}
	echo "</select><br />\n";
	
	echo $str["setup_quality"];
	echo "<select name=\"quality\" class=\"form\">\n";
	if (empty($_SESSION["s_data"]["upload_quality"])) $_SESSION["s_data"]["upload_quality"]=$default_quality;
	for ($i=1;$i<11;$i++){
		if (Trim($_SESSION["s_data"]["upload_quality"])==($i*10)){
			echo "<option value=\"".($i*10)."\" SELECTED>".($i*10)."%</option>\n";
		} else {
			echo "<option value=\"".($i*10)."\">".($i*10)."%</option>\n";
		}
	}
	echo "</select>\n";
	
	echo "<br /><input type=\"submit\" name=\"submit\" value=\"".$str["adm_new_file_submit"]."\" class=\"form\" /><br />\n";
	echo "</form><br />\n";
	echo "<a href=\"index.php?page=\">".$str["adm_storno"]."</a><br /><br />\n";
} else {
	// create...
	$_SESSION["s_data"]["upload_res"]=$_POST["new_res"];
	$_SESSION["s_data"]["upload_quality"]=$_POST["quality"];
	for ($i=0;$i<$max;$i++){
		$file_id="file_name_".$i;
		if (!empty($_FILES[$file_id]["name"])){
			if ($_POST["new_res"]=="orig"){
				//DO NOT RESAMPLE
				if (is_uploaded_file($_FILES[$file_id]["tmp_name"])) {
					move_uploaded_file($_FILES[$file_id]["tmp_name"], "./_images".$_SESSION["s_data"]["dir"].$_FILES[$file_id]["name"]);
					$text=str_replace("%1",$_FILES[$file_id]["name"],$str["adm_new_file_ok"]);
				} else {
					$text=str_replace("%1",$_FILES[$file_id]["name"],$str["adm_new_file_error"]);
				}
			} else {
				// RESAMPLE
				if (is_uploaded_file($_FILES[$file_id]["tmp_name"])) {
					List($resx,$resy)=Explode("x",$_POST["new_res"]);
					$file=$_FILES[$file_id]["tmp_name"];
					@$info=getimagesize($file);
					if ($info[0]<$info[1]){
						@$resx = floor($info[0]/($info[1]/$resy)); // kvuli zarovnani velikostnich pomeru 
					} else {
						@$resy = floor($info[1]/($info[0]/$resx)); // kvuli zarovnani velikostnich pomeru 
					}
					if (function_exists("imagecreatetruecolor")){
						if (!@$img=imagecreatetruecolor($resx,$resy)){
							$img=imagecreate($resx,$resy);
						}
					} else {
						$img=imagecreate($resx,$resy);
					}
					if ($info[2]==1){
						$img_in=@imagecreatefromgif($file);
					} else if ($info[2]==2){
						$img_in=@imagecreatefromjpeg($file);
					} else if ($info[2]==3){
						$img_in=@imagecreatefrompng($file);
					}
					
					if (function_exists("imagecopyresampled")){
						if (!@imagecopyresampled($img,$img_in,0,0,1,1,$resx,$resy,$info[0],$info[1])){
							@imagecopyresized($img,$img_in,0,0,1,1,$resx,$resy,$info[0],$info[1]);
						}
					} else {
						@imagecopyresized($img,$img_in,0,0,1,1,$resx,$resy,$info[0],$info[1]);
					}
					$new_file="./_images".$_SESSION["s_data"]["dir"].$_FILES[$file_id]["name"];
					if (@ImageJpeg($img,$new_file,$_POST["quality"])){
						$text=str_replace("%1",$_FILES[$file_id]["name"],$str["adm_new_file_ok"]);
					} else {
						$text=str_replace("%1",$_FILES[$file_id]["name"],$str["adm_new_file_error"]);
					}
				}
			}
			echo $text;
			echo "<br />\n";
		}
	}
	echo "<h1><a href=\"index.php?page=\">".$str["adm_continue"]."</a></h1>\n";
}
?>


